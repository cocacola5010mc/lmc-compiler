const fs = require('fs') // EWWW es6 imports plz
const Lexer = require('./lexer.js')

const cwd = process.cwd() // This may change? Either way, better to put it in a constant rather than call it multiple
// times becasue node js shouldnt be optimising pure functions

const getPath = (string) => {
  if (string[0] === '/') {
    // This just checks if the user is using a absolunt path rather than a relative one
    return string
  } else {
    return `${cwd}/${string}`
  }
}

// For now im going to assume that the first (2) and second (3) input arguments are input and output respectively
const inputFilePath = getPath(process.argv[2])
const outputFilePath = getPath(process.argv[3])

const inputFile = fs.readFileSync(inputFilePath).toString()

console.log(inputFile)

const inputArray = Lexer.makeInputArray(inputFile)

console.log(inputArray)
