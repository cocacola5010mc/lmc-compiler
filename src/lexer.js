class Lexer {
  static makeInputArray(inputFile) {
    return inputFile.split('\n')
  }
  static isolateFunctions(inputArray) {
    // TODO: Make a test for this. It is fairly safe to assume that it works tho

    const getFunctionName = (ln) => ln.split(' ')[1]
    const getArguments = (ln) => ln.split('(')[1].split(')')[0].split(' ') // TODO: Use regex here to make this cleaner
    const isIndented = (ln) => ln.startsWith('  ')

    let functionIndexes = []
    inputArray.forEach((line, index) => {
      if (line.startsWith('function')) {
        functionIndexes.push(index)
      }
    })

    let functions = []

    functionIndexes.forEach((lineIndex) => {
      let functionArray = []

      for (let i = lineIndex + 1; i <= inputArray.length; ++i) {
        if (isIndented(inputArray[i]) || line === '') {
          functionArray.push(inputArray[i])
        } else {
          break
        }
      }

      functions.push({
        name: getFunctionName(inputArray[lineIndex]),
        inputs: getArguments(inputArray[lineIndex]),
        data: functionArray,
      })
    })
  }
  static generateCalls(linesArray) {
    // This function is making a tree of calls from function calls and returns.
    /* Functions can be called like the following:
     * myFun arg1 arg2
     * myFun arg1
     * myFun1 arg1 $ myFun2 arg1
     */
    return linesArray
      .filter((line) => line !== '')
      .map((line) => {
        const [functionName, ...args] = line.split('$').reverse().map((word) => word.split(' '))
        return { name: functionName, args: args }
      })
  }
  static getVariables(inputArray) {
    let variables = []
    inputArray.forEach((line) => {
      if (line.includes('variable')) {
        const [name, value] = line.split(' ')
        variables.push({ name: name, value: value })
      }
    })
  }
}
module.exports = Lexer
