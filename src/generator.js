/*
 * Functions that I want
 *
 * print
 * add
 * subtract
 * increment
 * decrement
 * input
 * multiply
 * devide
 * 
 * Commands that I want
 *
 * countDown
 * countUp
 * if
 *
 * There will be io vars for each function that get made for each of them
 * 
 */
const lib = {
  print: 
    [
      ["PRINTI0"],
      ["OUT PRINTI0"]
    ],
}


class Generator {
  generateCalls(array) {
    /*
     * So, This is given an array, it needs to manage the IO of the function.
     *
     * Functions can be called like the following:
     * myFun arg1 arg2
     * myFun arg1
     * myFun1 arg1 $ myFun2 arg1
     * 
     * The format of the array is, split at the dollar, reverse, map (split at spaces)
     * This means that the first item of the sub arrays is the function, and the rest are args. The first item in each
     * array are the function names. 
     *
     * The array is in a reverse order so the first of this array is the last arg of the next. For this, the function
     * implementation functions need to return the names of the IO variables
     */
  }
}

module.exports = Generator
